import * as React from "react";
import { Helmet } from "react-helmet";
import { Link } from "react-router-dom";
import Button from '@material-ui/core/Button';
import './page404.scss';

const Page404 = () => (
  <>
    <Helmet>
      <meta charSet="utf-8" />
      <title>Application page not found screen</title>
      <link rel="canonical" href={window.location.href} />
    </Helmet>
    <div className="mainbox">
      <div className="err">4</div>
      <i className="far fa-question-circle fa-spin"></i>
      <div className="err2">4</div>
      <div className="msg">
        Maybe this page moved? Got deleted? Is hiding out in quarantine? Never existed in the first place?
        <p>Let's go 
        <Button
          type="submit"
          fullWidth
          variant="contained"
          color="primary"
        >
          <Link to="/" style={{color:"#FFF", textDecoration: 'none'}}>Go to Home</Link>
        </Button>
        and try from there.
        </p>
      </div>
    </div>
  </>
);

export default React.memo(Page404);