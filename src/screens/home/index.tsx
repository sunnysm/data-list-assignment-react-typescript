import * as React from "react";
import { Helmet } from "react-helmet";
import { Container } from '@material-ui/core';
import StatReport from '../../sections/stats';

const HomeScreen = React.memo(() => {
  return (
    <>
      <Helmet>
        <meta charSet="utf-8" />
        <title>Application dashbord screen</title>
        <link rel="canonical" href={window.location.href} />
      </Helmet>
      <Container component="div" maxWidth="md">
        <StatReport />
      </Container>
    </>
  );
});

export default HomeScreen;

