import moment from 'moment';

export const formatedDate = (date:string) => moment(date).format('MMM DD, YYYY');