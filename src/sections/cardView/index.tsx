import React from 'react';
import { makeStyles, Theme, createStyles } from '@material-ui/core/styles';
import Card from '@material-ui/core/Card';
import CardHeader from '@material-ui/core/CardHeader';
import CardContent from '@material-ui/core/CardContent';
import CardActions from '@material-ui/core/CardActions';
import IconButton from '@material-ui/core/IconButton';
import Typography from '@material-ui/core/Typography';
import FavoriteIcon from '@material-ui/icons/Favorite';
import ShareIcon from '@material-ui/icons/Share';
import MoreVertIcon from '@material-ui/icons/MoreVert';
import Box from '@material-ui/core/Box';
import { IStatRecord } from '../../model/app';
import { formatedDate } from '../../helper';

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    root: {
      maxWidth: '32%',
      width: '23%',
      margin: '4px 9px'
    },
    media: {
      height: 0,
      paddingTop: '56.25%', // 16:9
    },
  }),
);

const CardView = (props:any) => {
  const classes = useStyles();
  const { data } = props;
  return (
    <div style={{ width: '100%' }}>
      <Box display="flex" flexDirection="row" flexWrap="wrap" alignItems="flex-start">
        {data.map((stat:IStatRecord, index:number) => (
          <Card className={classes.root} key={`stat_${index}`}>
            <CardHeader
                action={
                <IconButton aria-label="settings">
                    <MoreVertIcon />
                </IconButton>
                }
                title="Financial"
                subheader={formatedDate(stat.Date)}
            />
            <CardContent>
                <Typography variant="body1" color="textSecondary" component="div">
                 <strong>Open : </strong>
                 {stat.Open}
                </Typography>
                <Typography variant="body1" color="textSecondary" component="div">
                 <strong>High : </strong>
                 {stat.High}
                </Typography>
                <Typography variant="body1" color="textSecondary" component="div">
                 <strong>Low : </strong>
                 {stat.Low}
                </Typography>
                <Typography variant="body1" color="textSecondary" component="div">
                 <strong>Close : </strong>
                 {stat.Close}
                </Typography>
            </CardContent>
            <CardActions disableSpacing>
                <IconButton aria-label="add to favorites">
                <FavoriteIcon />
                </IconButton>
                <IconButton aria-label="share">
                <ShareIcon />
                </IconButton>
            </CardActions>
            </Card>
        ))}
      </Box>
    </div>
  );
}

export default React.memo(CardView);