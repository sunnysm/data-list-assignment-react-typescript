import * as React from 'react';
import IconButton from '@material-ui/core/IconButton';
import ViewListIcon from '@material-ui/icons/ViewList';
import ViewModuleIcon from '@material-ui/icons/ViewModule';
import ListView from '../listView';
import CardView from '../cardView';
import stats from '../../data/gspc.json';
import Loading from '../../components/loader';

const StatReport = () => {
  const data:any = stats;
  const [view, setView] = React.useState('list');
  const [loader, setLoader] = React.useState(false);
  const template:any = (view === 'list') ? <ListView data={data} /> : <CardView data={data} />;

  const manageTemplate = (view:string) => {
    setLoader(true);
    setTimeout(() => {
      setLoader(false);
      setView(view)
    }, 1500)
  }
  return (
    <div>
      {loader && <Loading />}
      <div style={{textAlign: 'right'}}>
        <IconButton
          onClick={() => manageTemplate('list')}
          color={(view === 'list') ? 'primary':'inherit'}
        >
          <ViewListIcon fontSize="large" />
        </IconButton>
        <IconButton
          color={(view === 'card') ? 'primary':'inherit'}
          onClick={() => manageTemplate('card')} 
        >
          <ViewModuleIcon fontSize="large" />
        </IconButton>
      </div>
      {template}
    </div>
  )
}

export default StatReport;