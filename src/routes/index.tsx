import * as React from "react";
import { Router, Route, Switch } from "react-router-dom";
import Loading from "../components/loader";
import { appHistory } from "../app-history";

const Page404Screen = React.lazy(() => import("../screens/page404"));
const HomeScreen = React.lazy(() => import("../screens/home"));

const history = appHistory();
const Routing = () => (
  <React.Suspense fallback={<Loading />}>
    <Router history={history}>
      <Switch>
        <Route
          path="/"
          component={() => <HomeScreen />}
        />
        <Route path="/*" component={() => <Page404Screen />} />
      </Switch>
    </Router>
  </React.Suspense>
);

export default Routing;
